EESchema Schematic File Version 4
LIBS:smt breakout-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SMT Breakout Breadboard"
Date "2019-03-10"
Rev "1.0"
Comp "Sean Lanigan"
Comment1 "https://gitlab.com/nifty-pcbs/smt-breakout-breadboard"
Comment2 ""
Comment3 ""
Comment4 "Released under the CERN Open Hardware Licence v1.2"
$EndDescr
$Comp
L Connector:Conn_01x33_Male J1
U 1 1 5C6A94AF
P 750 3800
F 0 "J1" H 856 5578 50  0000 C CNN
F 1 "Vcc rail" H 856 5487 50  0000 C CNN
F 2 "custom-footprints:33-pin" H 750 3800 50  0001 C CNN
F 3 "~" H 750 3800 50  0001 C CNN
	1    750  3800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x33_Male J2
U 1 1 5C6A9538
P 1400 3800
F 0 "J2" H 1506 5578 50  0000 C CNN
F 1 "GND rail" H 1506 5487 50  0000 C CNN
F 2 "custom-footprints:33-pin" H 1400 3800 50  0001 C CNN
F 3 "~" H 1400 3800 50  0001 C CNN
	1    1400 3800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5C6A96A3
P 1000 1700
F 0 "H1" H 1100 1751 50  0000 L CNN
F 1 "Vcc" H 1100 1660 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_Pad_Via" H 1000 1700 50  0001 C CNN
F 3 "~" H 1000 1700 50  0001 C CNN
	1    1000 1700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5C6A9765
P 1650 5800
F 0 "H2" H 1550 5758 50  0000 R CNN
F 1 "GND" H 1550 5849 50  0000 R CNN
F 2 "MountingHole:MountingHole_4.3mm_M4_Pad_Via" H 1650 5800 50  0001 C CNN
F 3 "~" H 1650 5800 50  0001 C CNN
	1    1650 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	950  5400 1000 5400
Wire Wire Line
	950  5300 1000 5300
Connection ~ 1000 5300
Wire Wire Line
	1000 5300 1000 5400
Wire Wire Line
	950  5200 1000 5200
Connection ~ 1000 5200
Wire Wire Line
	1000 5200 1000 5300
Wire Wire Line
	950  5100 1000 5100
Connection ~ 1000 5100
Wire Wire Line
	1000 5100 1000 5200
Wire Wire Line
	950  5000 1000 5000
Connection ~ 1000 5000
Wire Wire Line
	1000 5000 1000 5100
Wire Wire Line
	950  4900 1000 4900
Connection ~ 1000 4900
Wire Wire Line
	1000 4900 1000 5000
Wire Wire Line
	950  4800 1000 4800
Connection ~ 1000 4800
Wire Wire Line
	1000 4800 1000 4900
Wire Wire Line
	950  4700 1000 4700
Connection ~ 1000 4700
Wire Wire Line
	1000 4700 1000 4800
Wire Wire Line
	950  4600 1000 4600
Connection ~ 1000 4600
Wire Wire Line
	1000 4600 1000 4700
Wire Wire Line
	950  4500 1000 4500
Connection ~ 1000 4500
Wire Wire Line
	1000 4500 1000 4600
Wire Wire Line
	950  4400 1000 4400
Connection ~ 1000 4400
Wire Wire Line
	1000 4400 1000 4500
Wire Wire Line
	950  4300 1000 4300
Connection ~ 1000 4300
Wire Wire Line
	1000 4300 1000 4400
Wire Wire Line
	950  4200 1000 4200
Connection ~ 1000 4200
Wire Wire Line
	1000 4200 1000 4300
Wire Wire Line
	950  4100 1000 4100
Connection ~ 1000 4100
Wire Wire Line
	1000 4100 1000 4200
Wire Wire Line
	950  4000 1000 4000
Connection ~ 1000 4000
Wire Wire Line
	1000 4000 1000 4100
Wire Wire Line
	950  3900 1000 3900
Connection ~ 1000 3900
Wire Wire Line
	1000 3900 1000 4000
Wire Wire Line
	950  3800 1000 3800
Connection ~ 1000 3800
Wire Wire Line
	1000 3800 1000 3900
Wire Wire Line
	950  3700 1000 3700
Connection ~ 1000 3700
Wire Wire Line
	1000 3700 1000 3800
Wire Wire Line
	950  3600 1000 3600
Connection ~ 1000 3600
Wire Wire Line
	1000 3600 1000 3700
Wire Wire Line
	950  3500 1000 3500
Connection ~ 1000 3500
Wire Wire Line
	1000 3500 1000 3600
Wire Wire Line
	950  3400 1000 3400
Connection ~ 1000 3400
Wire Wire Line
	1000 3400 1000 3500
Wire Wire Line
	950  3300 1000 3300
Connection ~ 1000 3300
Wire Wire Line
	1000 3300 1000 3400
Wire Wire Line
	950  3200 1000 3200
Connection ~ 1000 3200
Wire Wire Line
	1000 3200 1000 3300
Wire Wire Line
	950  3100 1000 3100
Connection ~ 1000 3100
Wire Wire Line
	1000 3100 1000 3200
Wire Wire Line
	950  3000 1000 3000
Connection ~ 1000 3000
Wire Wire Line
	1000 3000 1000 3100
Wire Wire Line
	950  2900 1000 2900
Connection ~ 1000 2900
Wire Wire Line
	1000 2900 1000 3000
Wire Wire Line
	950  2800 1000 2800
Connection ~ 1000 2800
Wire Wire Line
	1000 2800 1000 2900
Wire Wire Line
	950  2700 1000 2700
Connection ~ 1000 2700
Wire Wire Line
	1000 2700 1000 2800
Wire Wire Line
	950  2600 1000 2600
Connection ~ 1000 2600
Wire Wire Line
	1000 2600 1000 2700
Wire Wire Line
	950  2500 1000 2500
Connection ~ 1000 2500
Wire Wire Line
	1000 2500 1000 2600
Wire Wire Line
	950  2400 1000 2400
Connection ~ 1000 2400
Wire Wire Line
	1000 2400 1000 2500
Wire Wire Line
	950  2300 1000 2300
Connection ~ 1000 2300
Wire Wire Line
	1000 2300 1000 2400
Wire Wire Line
	950  2200 1000 2200
Connection ~ 1000 2200
Wire Wire Line
	1000 2200 1000 2300
Wire Wire Line
	1600 2200 1650 2200
Wire Wire Line
	1600 2300 1650 2300
Connection ~ 1650 2300
Wire Wire Line
	1650 2300 1650 2200
Wire Wire Line
	1600 2400 1650 2400
Connection ~ 1650 2400
Wire Wire Line
	1650 2400 1650 2300
Wire Wire Line
	1600 2500 1650 2500
Connection ~ 1650 2500
Wire Wire Line
	1650 2500 1650 2400
Wire Wire Line
	1600 2600 1650 2600
Connection ~ 1650 2600
Wire Wire Line
	1650 2600 1650 2500
Wire Wire Line
	1600 2700 1650 2700
Connection ~ 1650 2700
Wire Wire Line
	1650 2700 1650 2600
Wire Wire Line
	1600 2800 1650 2800
Connection ~ 1650 2800
Wire Wire Line
	1650 2800 1650 2700
Wire Wire Line
	1600 2900 1650 2900
Connection ~ 1650 2900
Wire Wire Line
	1650 2900 1650 2800
Wire Wire Line
	1600 3000 1650 3000
Connection ~ 1650 3000
Wire Wire Line
	1650 3000 1650 2900
Wire Wire Line
	1600 3100 1650 3100
Connection ~ 1650 3100
Wire Wire Line
	1650 3100 1650 3000
Wire Wire Line
	1600 3200 1650 3200
Connection ~ 1650 3200
Wire Wire Line
	1650 3200 1650 3100
Wire Wire Line
	1600 3300 1650 3300
Connection ~ 1650 3300
Wire Wire Line
	1650 3300 1650 3200
Wire Wire Line
	1600 3400 1650 3400
Connection ~ 1650 3400
Wire Wire Line
	1650 3400 1650 3300
Wire Wire Line
	1600 3500 1650 3500
Connection ~ 1650 3500
Wire Wire Line
	1650 3500 1650 3400
Wire Wire Line
	1600 3600 1650 3600
Connection ~ 1650 3600
Wire Wire Line
	1650 3600 1650 3500
Wire Wire Line
	1600 3700 1650 3700
Connection ~ 1650 3700
Wire Wire Line
	1650 3700 1650 3600
Wire Wire Line
	1600 3800 1650 3800
Connection ~ 1650 3800
Wire Wire Line
	1650 3800 1650 3700
Wire Wire Line
	1600 3900 1650 3900
Connection ~ 1650 3900
Wire Wire Line
	1650 3900 1650 3800
Wire Wire Line
	1600 4000 1650 4000
Connection ~ 1650 4000
Wire Wire Line
	1650 4000 1650 3900
Wire Wire Line
	1600 4100 1650 4100
Connection ~ 1650 4100
Wire Wire Line
	1650 4100 1650 4000
Wire Wire Line
	1600 4200 1650 4200
Connection ~ 1650 4200
Wire Wire Line
	1650 4200 1650 4100
Wire Wire Line
	1600 4300 1650 4300
Connection ~ 1650 4300
Wire Wire Line
	1650 4300 1650 4200
Wire Wire Line
	1600 4400 1650 4400
Connection ~ 1650 4400
Wire Wire Line
	1650 4400 1650 4300
Wire Wire Line
	1600 4500 1650 4500
Connection ~ 1650 4500
Wire Wire Line
	1650 4500 1650 4400
Wire Wire Line
	1600 4600 1650 4600
Connection ~ 1650 4600
Wire Wire Line
	1650 4600 1650 4500
Wire Wire Line
	1600 4700 1650 4700
Connection ~ 1650 4700
Wire Wire Line
	1650 4700 1650 4600
Wire Wire Line
	1600 4800 1650 4800
Connection ~ 1650 4800
Wire Wire Line
	1650 4800 1650 4700
Wire Wire Line
	1600 4900 1650 4900
Connection ~ 1650 4900
Wire Wire Line
	1650 4900 1650 4800
Wire Wire Line
	1600 5000 1650 5000
Connection ~ 1650 5000
Wire Wire Line
	1650 5000 1650 4900
Wire Wire Line
	1600 5100 1650 5100
Connection ~ 1650 5100
Wire Wire Line
	1650 5100 1650 5000
Wire Wire Line
	1600 5200 1650 5200
Connection ~ 1650 5200
Wire Wire Line
	1650 5200 1650 5100
Wire Wire Line
	1600 5300 1650 5300
Connection ~ 1650 5300
Wire Wire Line
	1650 5300 1650 5200
Wire Wire Line
	1600 5400 1650 5400
Connection ~ 1650 5400
Wire Wire Line
	1650 5400 1650 5300
$Comp
L MCU_Microchip_ATtiny:ATtiny4-TS U1
U 1 1 5C72D9BB
P 2900 1650
F 0 "U1" H 2370 1696 50  0000 R CNN
F 1 "SOT-23-6" H 2370 1605 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 2900 1650 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8127-AVR-8-bit-Microcontroller-ATtiny4-ATtiny5-ATtiny9-ATtiny10_Datasheet.pdf" H 2900 1650 50  0001 C CNN
	1    2900 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J3
U 1 1 5C72DB51
P 3150 900
F 0 "J3" H 3178 830 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3177 785 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3150 900 50  0001 C CNN
F 3 "~" H 3150 900 50  0001 C CNN
	1    3150 900 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J9
U 1 1 5C72DBBB
P 3900 1100
F 0 "J9" H 3928 1030 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 985 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 1100 50  0001 C CNN
F 3 "~" H 3900 1100 50  0001 C CNN
	1    3900 1100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J10
U 1 1 5C72DC20
P 3900 1350
F 0 "J10" H 3928 1280 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 1235 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 1350 50  0001 C CNN
F 3 "~" H 3900 1350 50  0001 C CNN
	1    3900 1350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J11
U 1 1 5C72DC76
P 3900 1600
F 0 "J11" H 3928 1530 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 1485 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 1600 50  0001 C CNN
F 3 "~" H 3900 1600 50  0001 C CNN
	1    3900 1600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J12
U 1 1 5C72DCC3
P 3900 1850
F 0 "J12" H 3928 1780 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 1735 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 1850 50  0001 C CNN
F 3 "~" H 3900 1850 50  0001 C CNN
	1    3900 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1050 2900 1000
Wire Wire Line
	2900 900  2950 900 
Wire Wire Line
	2950 1000 2900 1000
Connection ~ 2900 1000
Wire Wire Line
	2900 1000 2900 900 
Wire Wire Line
	3500 1350 3550 1350
Wire Wire Line
	3550 1350 3550 1200
Wire Wire Line
	3550 1100 3700 1100
Wire Wire Line
	3700 1200 3550 1200
Connection ~ 3550 1200
Wire Wire Line
	3550 1200 3550 1100
Wire Wire Line
	3500 1450 3600 1450
Wire Wire Line
	3600 1450 3600 1350
Wire Wire Line
	3600 1350 3700 1350
Wire Wire Line
	3700 1450 3600 1450
Connection ~ 3600 1450
Wire Wire Line
	3500 1550 3600 1550
Wire Wire Line
	3600 1550 3600 1600
Wire Wire Line
	3600 1700 3700 1700
Wire Wire Line
	3700 1600 3600 1600
Connection ~ 3600 1600
Wire Wire Line
	3600 1600 3600 1700
Wire Wire Line
	3500 1650 3550 1650
Wire Wire Line
	3550 1650 3550 1850
Wire Wire Line
	3550 1950 3700 1950
Wire Wire Line
	3700 1850 3550 1850
Connection ~ 3550 1850
Wire Wire Line
	3550 1850 3550 1950
$Comp
L Connector:Conn_01x02_Female J4
U 1 1 5C7695D5
P 3150 2300
F 0 "J4" H 3178 2230 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3177 2185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3150 2300 50  0001 C CNN
F 3 "~" H 3150 2300 50  0001 C CNN
	1    3150 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2250 2900 2300
Wire Wire Line
	2900 2400 2950 2400
Wire Wire Line
	2950 2300 2900 2300
Connection ~ 2900 2300
Wire Wire Line
	2900 2300 2900 2400
$Comp
L MCU_Microchip_ATtiny:ATtiny4-TS U2
U 1 1 5C77C748
P 2900 3350
F 0 "U2" H 2370 3396 50  0000 R CNN
F 1 "SOT-23-6" H 2370 3305 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 2900 3350 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8127-AVR-8-bit-Microcontroller-ATtiny4-ATtiny5-ATtiny9-ATtiny10_Datasheet.pdf" H 2900 3350 50  0001 C CNN
	1    2900 3350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J5
U 1 1 5C77C74E
P 3150 2600
F 0 "J5" H 3178 2530 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3177 2485 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3150 2600 50  0001 C CNN
F 3 "~" H 3150 2600 50  0001 C CNN
	1    3150 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J13
U 1 1 5C77C754
P 3900 2800
F 0 "J13" H 3928 2730 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 2685 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 2800 50  0001 C CNN
F 3 "~" H 3900 2800 50  0001 C CNN
	1    3900 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J14
U 1 1 5C77C75A
P 3900 3050
F 0 "J14" H 3928 2980 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 2935 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 3050 50  0001 C CNN
F 3 "~" H 3900 3050 50  0001 C CNN
	1    3900 3050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J15
U 1 1 5C77C760
P 3900 3300
F 0 "J15" H 3928 3230 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 3185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 3300 50  0001 C CNN
F 3 "~" H 3900 3300 50  0001 C CNN
	1    3900 3300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J16
U 1 1 5C77C766
P 3900 3550
F 0 "J16" H 3928 3480 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 3435 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 3550 50  0001 C CNN
F 3 "~" H 3900 3550 50  0001 C CNN
	1    3900 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2750 2900 2700
Wire Wire Line
	2900 2600 2950 2600
Wire Wire Line
	2950 2700 2900 2700
Connection ~ 2900 2700
Wire Wire Line
	2900 2700 2900 2600
Wire Wire Line
	3500 3050 3550 3050
Wire Wire Line
	3550 3050 3550 2900
Wire Wire Line
	3550 2800 3700 2800
Wire Wire Line
	3700 2900 3550 2900
Connection ~ 3550 2900
Wire Wire Line
	3550 2900 3550 2800
Wire Wire Line
	3500 3150 3600 3150
Wire Wire Line
	3600 3150 3600 3050
Wire Wire Line
	3600 3050 3700 3050
Wire Wire Line
	3700 3150 3600 3150
Connection ~ 3600 3150
Wire Wire Line
	3500 3250 3600 3250
Wire Wire Line
	3600 3250 3600 3300
Wire Wire Line
	3600 3400 3700 3400
Wire Wire Line
	3700 3300 3600 3300
Connection ~ 3600 3300
Wire Wire Line
	3600 3300 3600 3400
Wire Wire Line
	3500 3350 3550 3350
Wire Wire Line
	3550 3350 3550 3550
Wire Wire Line
	3550 3650 3700 3650
Wire Wire Line
	3700 3550 3550 3550
Connection ~ 3550 3550
Wire Wire Line
	3550 3550 3550 3650
$Comp
L Connector:Conn_01x02_Female J6
U 1 1 5C77C788
P 3150 4000
F 0 "J6" H 3178 3930 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3177 3885 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3150 4000 50  0001 C CNN
F 3 "~" H 3150 4000 50  0001 C CNN
	1    3150 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3950 2900 4000
Wire Wire Line
	2900 4100 2950 4100
Wire Wire Line
	2950 4000 2900 4000
Connection ~ 2900 4000
Wire Wire Line
	2900 4000 2900 4100
$Comp
L MCU_Microchip_ATtiny:ATtiny4-TS U3
U 1 1 5C785115
P 2900 5050
F 0 "U3" H 2370 5096 50  0000 R CNN
F 1 "SOT-23-6" H 2370 5005 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 2900 5050 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8127-AVR-8-bit-Microcontroller-ATtiny4-ATtiny5-ATtiny9-ATtiny10_Datasheet.pdf" H 2900 5050 50  0001 C CNN
	1    2900 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J7
U 1 1 5C78511B
P 3150 4300
F 0 "J7" H 3178 4230 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3177 4185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3150 4300 50  0001 C CNN
F 3 "~" H 3150 4300 50  0001 C CNN
	1    3150 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J17
U 1 1 5C785121
P 3900 4500
F 0 "J17" H 3928 4430 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 4385 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 4500 50  0001 C CNN
F 3 "~" H 3900 4500 50  0001 C CNN
	1    3900 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J18
U 1 1 5C785127
P 3900 4750
F 0 "J18" H 3928 4680 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 4635 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 4750 50  0001 C CNN
F 3 "~" H 3900 4750 50  0001 C CNN
	1    3900 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J19
U 1 1 5C78512D
P 3900 5000
F 0 "J19" H 3928 4930 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 4885 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 5000 50  0001 C CNN
F 3 "~" H 3900 5000 50  0001 C CNN
	1    3900 5000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J20
U 1 1 5C785133
P 3900 5250
F 0 "J20" H 3928 5180 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3927 5135 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3900 5250 50  0001 C CNN
F 3 "~" H 3900 5250 50  0001 C CNN
	1    3900 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4450 2900 4400
Wire Wire Line
	2900 4300 2950 4300
Wire Wire Line
	2950 4400 2900 4400
Connection ~ 2900 4400
Wire Wire Line
	2900 4400 2900 4300
Wire Wire Line
	3500 4750 3550 4750
Wire Wire Line
	3550 4750 3550 4600
Wire Wire Line
	3550 4500 3700 4500
Wire Wire Line
	3700 4600 3550 4600
Connection ~ 3550 4600
Wire Wire Line
	3550 4600 3550 4500
Wire Wire Line
	3500 4850 3600 4850
Wire Wire Line
	3600 4850 3600 4750
Wire Wire Line
	3600 4750 3700 4750
Wire Wire Line
	3700 4850 3600 4850
Connection ~ 3600 4850
Wire Wire Line
	3500 4950 3600 4950
Wire Wire Line
	3600 4950 3600 5000
Wire Wire Line
	3600 5100 3700 5100
Wire Wire Line
	3700 5000 3600 5000
Connection ~ 3600 5000
Wire Wire Line
	3600 5000 3600 5100
Wire Wire Line
	3500 5050 3550 5050
Wire Wire Line
	3550 5050 3550 5250
Wire Wire Line
	3550 5350 3700 5350
Wire Wire Line
	3700 5250 3550 5250
Connection ~ 3550 5250
Wire Wire Line
	3550 5250 3550 5350
$Comp
L Connector:Conn_01x02_Female J8
U 1 1 5C785155
P 3150 5700
F 0 "J8" H 3178 5630 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3177 5585 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 3150 5700 50  0001 C CNN
F 3 "~" H 3150 5700 50  0001 C CNN
	1    3150 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 5650 2900 5700
Wire Wire Line
	2900 5800 2950 5800
Wire Wire Line
	2950 5700 2900 5700
Connection ~ 2900 5700
Wire Wire Line
	2900 5700 2900 5800
Wire Wire Line
	1000 1800 1000 2200
$Comp
L Device:R R1
U 1 1 5C79AF14
P 4950 900
F 0 "R1" V 4743 900 50  0000 C CNN
F 1 "0603-1206" V 4834 900 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 900 50  0001 C CNN
F 3 "~" H 4950 900 50  0001 C CNN
	1    4950 900 
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J22
U 1 1 5C79B010
P 5400 900
F 0 "J22" H 5428 830 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 785 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 900 50  0001 C CNN
F 3 "~" H 5400 900 50  0001 C CNN
	1    5400 900 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J21
U 1 1 5C79B0FF
P 4500 1000
F 0 "J21" H 4394 767 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 766 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 1000 50  0001 C CNN
F 3 "~" H 4500 1000 50  0001 C CNN
	1    4500 1000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 1000 4750 1000
Wire Wire Line
	4750 1000 4750 900 
Wire Wire Line
	4750 900  4800 900 
Wire Wire Line
	4750 900  4700 900 
Connection ~ 4750 900 
Wire Wire Line
	5100 900  5150 900 
Wire Wire Line
	5200 1000 5150 1000
Wire Wire Line
	5150 1000 5150 900 
Connection ~ 5150 900 
Wire Wire Line
	5150 900  5200 900 
$Comp
L Device:R R2
U 1 1 5C7D9C39
P 4950 1300
F 0 "R2" V 4743 1300 50  0000 C CNN
F 1 "0603-1206" V 4834 1300 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 1300 50  0001 C CNN
F 3 "~" H 4950 1300 50  0001 C CNN
	1    4950 1300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J26
U 1 1 5C7D9C3F
P 5400 1300
F 0 "J26" H 5428 1230 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 1185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 1300 50  0001 C CNN
F 3 "~" H 5400 1300 50  0001 C CNN
	1    5400 1300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J23
U 1 1 5C7D9C45
P 4500 1400
F 0 "J23" H 4394 1167 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 1166 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 1400 50  0001 C CNN
F 3 "~" H 4500 1400 50  0001 C CNN
	1    4500 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 1400 4750 1400
Wire Wire Line
	4750 1400 4750 1300
Wire Wire Line
	4750 1300 4800 1300
Wire Wire Line
	4750 1300 4700 1300
Connection ~ 4750 1300
Wire Wire Line
	5100 1300 5150 1300
Wire Wire Line
	5200 1400 5150 1400
Wire Wire Line
	5150 1400 5150 1300
Connection ~ 5150 1300
Wire Wire Line
	5150 1300 5200 1300
$Comp
L Device:R R3
U 1 1 5C7E51ED
P 4950 1650
F 0 "R3" V 4743 1650 50  0000 C CNN
F 1 "0603-1206" V 4834 1650 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 1650 50  0001 C CNN
F 3 "~" H 4950 1650 50  0001 C CNN
	1    4950 1650
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J27
U 1 1 5C7E51F3
P 5400 1650
F 0 "J27" H 5428 1580 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 1535 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 1650 50  0001 C CNN
F 3 "~" H 5400 1650 50  0001 C CNN
	1    5400 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J24
U 1 1 5C7E51F9
P 4500 1750
F 0 "J24" H 4394 1517 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 1516 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 1750 50  0001 C CNN
F 3 "~" H 4500 1750 50  0001 C CNN
	1    4500 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 1750 4750 1750
Wire Wire Line
	4750 1750 4750 1650
Wire Wire Line
	4750 1650 4800 1650
Wire Wire Line
	4750 1650 4700 1650
Connection ~ 4750 1650
Wire Wire Line
	5100 1650 5150 1650
Wire Wire Line
	5200 1750 5150 1750
Wire Wire Line
	5150 1750 5150 1650
Connection ~ 5150 1650
Wire Wire Line
	5150 1650 5200 1650
$Comp
L Device:R R4
U 1 1 5C7E5209
P 4950 2050
F 0 "R4" V 4743 2050 50  0000 C CNN
F 1 "0603-1206" V 4834 2050 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 2050 50  0001 C CNN
F 3 "~" H 4950 2050 50  0001 C CNN
	1    4950 2050
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J28
U 1 1 5C7E520F
P 5400 2050
F 0 "J28" H 5428 1980 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 1935 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 2050 50  0001 C CNN
F 3 "~" H 5400 2050 50  0001 C CNN
	1    5400 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J25
U 1 1 5C7E5215
P 4500 2150
F 0 "J25" H 4394 1917 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 1916 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 2150 50  0001 C CNN
F 3 "~" H 4500 2150 50  0001 C CNN
	1    4500 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 2150 4750 2150
Wire Wire Line
	4750 2150 4750 2050
Wire Wire Line
	4750 2050 4800 2050
Wire Wire Line
	4750 2050 4700 2050
Connection ~ 4750 2050
Wire Wire Line
	5100 2050 5150 2050
Wire Wire Line
	5200 2150 5150 2150
Wire Wire Line
	5150 2150 5150 2050
Connection ~ 5150 2050
Wire Wire Line
	5150 2050 5200 2050
$Comp
L Device:R R5
U 1 1 5C7F2761
P 4950 2400
F 0 "R5" V 4743 2400 50  0000 C CNN
F 1 "0603-1206" V 4834 2400 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 2400 50  0001 C CNN
F 3 "~" H 4950 2400 50  0001 C CNN
	1    4950 2400
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J32
U 1 1 5C7F2767
P 5400 2400
F 0 "J32" H 5428 2330 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 2285 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 2400 50  0001 C CNN
F 3 "~" H 5400 2400 50  0001 C CNN
	1    5400 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J29
U 1 1 5C7F276D
P 4500 2500
F 0 "J29" H 4394 2267 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 2266 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 2500 50  0001 C CNN
F 3 "~" H 4500 2500 50  0001 C CNN
	1    4500 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 2500 4750 2500
Wire Wire Line
	4750 2500 4750 2400
Wire Wire Line
	4750 2400 4800 2400
Wire Wire Line
	4750 2400 4700 2400
Connection ~ 4750 2400
Wire Wire Line
	5100 2400 5150 2400
Wire Wire Line
	5200 2500 5150 2500
Wire Wire Line
	5150 2500 5150 2400
Connection ~ 5150 2400
Wire Wire Line
	5150 2400 5200 2400
$Comp
L Device:R R6
U 1 1 5C7F277D
P 4950 2750
F 0 "R6" V 4743 2750 50  0000 C CNN
F 1 "0603-1206" V 4834 2750 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 2750 50  0001 C CNN
F 3 "~" H 4950 2750 50  0001 C CNN
	1    4950 2750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J33
U 1 1 5C7F2783
P 5400 2750
F 0 "J33" H 5428 2680 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 2635 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 2750 50  0001 C CNN
F 3 "~" H 5400 2750 50  0001 C CNN
	1    5400 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J30
U 1 1 5C7F2789
P 4500 2850
F 0 "J30" H 4394 2617 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 2616 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 2850 50  0001 C CNN
F 3 "~" H 4500 2850 50  0001 C CNN
	1    4500 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 2850 4750 2850
Wire Wire Line
	4750 2850 4750 2750
Wire Wire Line
	4750 2750 4800 2750
Wire Wire Line
	4750 2750 4700 2750
Connection ~ 4750 2750
Wire Wire Line
	5100 2750 5150 2750
Wire Wire Line
	5200 2850 5150 2850
Wire Wire Line
	5150 2850 5150 2750
Connection ~ 5150 2750
Wire Wire Line
	5150 2750 5200 2750
$Comp
L Device:R R7
U 1 1 5C7F2799
P 4950 3150
F 0 "R7" V 4743 3150 50  0000 C CNN
F 1 "0603-1206" V 4834 3150 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 3150 50  0001 C CNN
F 3 "~" H 4950 3150 50  0001 C CNN
	1    4950 3150
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J34
U 1 1 5C7F279F
P 5400 3150
F 0 "J34" H 5428 3080 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 3035 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 3150 50  0001 C CNN
F 3 "~" H 5400 3150 50  0001 C CNN
	1    5400 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J31
U 1 1 5C7F27A5
P 4500 3250
F 0 "J31" H 4394 3017 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 3016 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 3250 50  0001 C CNN
F 3 "~" H 4500 3250 50  0001 C CNN
	1    4500 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 3250 4750 3250
Wire Wire Line
	4750 3250 4750 3150
Wire Wire Line
	4750 3150 4800 3150
Wire Wire Line
	4750 3150 4700 3150
Connection ~ 4750 3150
Wire Wire Line
	5100 3150 5150 3150
Wire Wire Line
	5200 3250 5150 3250
Wire Wire Line
	5150 3250 5150 3150
Connection ~ 5150 3150
Wire Wire Line
	5150 3150 5200 3150
$Comp
L Device:R R8
U 1 1 5C6F10F4
P 4950 3550
F 0 "R8" V 4743 3550 50  0000 C CNN
F 1 "0603-1206" V 4834 3550 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 3550 50  0001 C CNN
F 3 "~" H 4950 3550 50  0001 C CNN
	1    4950 3550
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J37
U 1 1 5C6F10FA
P 5400 3550
F 0 "J37" H 5428 3480 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 3435 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 3550 50  0001 C CNN
F 3 "~" H 5400 3550 50  0001 C CNN
	1    5400 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J35
U 1 1 5C6F1100
P 4500 3650
F 0 "J35" H 4394 3417 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 3416 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 3650 50  0001 C CNN
F 3 "~" H 4500 3650 50  0001 C CNN
	1    4500 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 3650 4750 3650
Wire Wire Line
	4750 3650 4750 3550
Wire Wire Line
	4750 3550 4800 3550
Wire Wire Line
	4750 3550 4700 3550
Connection ~ 4750 3550
Wire Wire Line
	5100 3550 5150 3550
Wire Wire Line
	5200 3650 5150 3650
Wire Wire Line
	5150 3650 5150 3550
Connection ~ 5150 3550
Wire Wire Line
	5150 3550 5200 3550
$Comp
L Device:R R9
U 1 1 5C6F1110
P 4950 3950
F 0 "R9" V 4743 3950 50  0000 C CNN
F 1 "0603-1206" V 4834 3950 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 3950 50  0001 C CNN
F 3 "~" H 4950 3950 50  0001 C CNN
	1    4950 3950
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J38
U 1 1 5C6F1116
P 5400 3950
F 0 "J38" H 5428 3880 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 3835 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 3950 50  0001 C CNN
F 3 "~" H 5400 3950 50  0001 C CNN
	1    5400 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J36
U 1 1 5C6F111C
P 4500 4050
F 0 "J36" H 4394 3817 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 3816 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 4050 50  0001 C CNN
F 3 "~" H 4500 4050 50  0001 C CNN
	1    4500 4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 4050 4750 4050
Wire Wire Line
	4750 4050 4750 3950
Wire Wire Line
	4750 3950 4800 3950
Wire Wire Line
	4750 3950 4700 3950
Connection ~ 4750 3950
Wire Wire Line
	5100 3950 5150 3950
Wire Wire Line
	5200 4050 5150 4050
Wire Wire Line
	5150 4050 5150 3950
Connection ~ 5150 3950
Wire Wire Line
	5150 3950 5200 3950
$Comp
L 74xx:74HC00 U4
U 1 1 5C7147DB
P 8400 900
F 0 "U4" H 8400 1225 50  0000 C CNN
F 1 "74HC00" H 8400 1134 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8400 900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8400 900 50  0001 C CNN
	1    8400 900 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U4
U 2 1 5C714ABE
P 8400 1450
F 0 "U4" H 8400 1775 50  0000 C CNN
F 1 "74HC00" H 8400 1684 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8400 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8400 1450 50  0001 C CNN
	2    8400 1450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U4
U 3 1 5C714D23
P 8400 2000
F 0 "U4" H 8400 2325 50  0000 C CNN
F 1 "74HC00" H 8400 2234 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8400 2000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8400 2000 50  0001 C CNN
	3    8400 2000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U4
U 4 1 5C714E0D
P 10150 1350
F 0 "U4" H 10150 1675 50  0000 C CNN
F 1 "74HC00" H 10150 1584 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 10150 1350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 10150 1350 50  0001 C CNN
	4    10150 1350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U4
U 5 1 5C714F50
P 10200 2000
F 0 "U4" V 9833 2000 50  0000 C CNN
F 1 "74HC00" V 9924 2000 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 10200 2000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 10200 2000 50  0001 C CNN
	5    10200 2000
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J45
U 1 1 5C7381CA
P 9000 800
F 0 "J45" H 9028 730 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9027 685 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9000 800 50  0001 C CNN
F 3 "~" H 9000 800 50  0001 C CNN
	1    9000 800 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J46
U 1 1 5C749790
P 9000 1350
F 0 "J46" H 9028 1280 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9027 1235 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9000 1350 50  0001 C CNN
F 3 "~" H 9000 1350 50  0001 C CNN
	1    9000 1350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J47
U 1 1 5C75AD02
P 9000 1900
F 0 "J47" H 9028 1830 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9027 1785 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9000 1900 50  0001 C CNN
F 3 "~" H 9000 1900 50  0001 C CNN
	1    9000 1900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J39
U 1 1 5C82B6D6
P 7800 800
F 0 "J39" H 7828 730 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 685 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 800 50  0001 C CNN
F 3 "~" H 7800 800 50  0001 C CNN
	1    7800 800 
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J40
U 1 1 5C83CDEA
P 7800 1100
F 0 "J40" H 7828 1030 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 985 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 1100 50  0001 C CNN
F 3 "~" H 7800 1100 50  0001 C CNN
	1    7800 1100
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J41
U 1 1 5C83CE88
P 7800 1350
F 0 "J41" H 7828 1280 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 1235 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 1350 50  0001 C CNN
F 3 "~" H 7800 1350 50  0001 C CNN
	1    7800 1350
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J42
U 1 1 5C83CF1C
P 7800 1650
F 0 "J42" H 7828 1580 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 1535 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 1650 50  0001 C CNN
F 3 "~" H 7800 1650 50  0001 C CNN
	1    7800 1650
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J43
U 1 1 5C83CFB4
P 7800 1900
F 0 "J43" H 7828 1830 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 1785 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 1900 50  0001 C CNN
F 3 "~" H 7800 1900 50  0001 C CNN
	1    7800 1900
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J44
U 1 1 5C83D04C
P 7800 2200
F 0 "J44" H 7828 2130 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 2085 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 2200 50  0001 C CNN
F 3 "~" H 7800 2200 50  0001 C CNN
	1    7800 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	8000 700  8050 700 
Wire Wire Line
	8050 700  8050 800 
Wire Wire Line
	8050 800  8100 800 
Wire Wire Line
	8050 800  8000 800 
Connection ~ 8050 800 
Wire Wire Line
	8000 1000 8050 1000
Wire Wire Line
	8000 1100 8050 1100
Wire Wire Line
	8050 1100 8050 1000
Connection ~ 8050 1000
Wire Wire Line
	8050 1000 8100 1000
Wire Wire Line
	8000 1350 8050 1350
Wire Wire Line
	8000 1250 8050 1250
Wire Wire Line
	8050 1250 8050 1350
Connection ~ 8050 1350
Wire Wire Line
	8050 1350 8100 1350
Wire Wire Line
	8000 1550 8050 1550
Wire Wire Line
	8000 1650 8050 1650
Wire Wire Line
	8050 1650 8050 1550
Connection ~ 8050 1550
Wire Wire Line
	8050 1550 8100 1550
Wire Wire Line
	8000 1800 8050 1800
Wire Wire Line
	8050 1800 8050 1900
Wire Wire Line
	8050 1900 8100 1900
Wire Wire Line
	8050 1900 8000 1900
Connection ~ 8050 1900
Wire Wire Line
	8000 2100 8050 2100
Wire Wire Line
	8050 2100 8050 2200
Wire Wire Line
	8050 2200 8000 2200
Connection ~ 8050 2100
Wire Wire Line
	8050 2100 8100 2100
Wire Wire Line
	8700 2000 8750 2000
Wire Wire Line
	8800 1900 8750 1900
Wire Wire Line
	8750 1900 8750 2000
Connection ~ 8750 2000
Wire Wire Line
	8750 2000 8800 2000
Wire Wire Line
	8800 1450 8750 1450
Wire Wire Line
	8800 1350 8750 1350
Wire Wire Line
	8750 1350 8750 1450
Connection ~ 8750 1450
Wire Wire Line
	8750 1450 8700 1450
Wire Wire Line
	8800 900  8750 900 
Wire Wire Line
	8800 800  8750 800 
Wire Wire Line
	8750 800  8750 900 
Connection ~ 8750 900 
Wire Wire Line
	8750 900  8700 900 
$Comp
L Connector:Conn_01x02_Female J49
U 1 1 5C99E9E7
P 9550 1250
F 0 "J49" H 9578 1180 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9577 1135 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9550 1250 50  0001 C CNN
F 3 "~" H 9550 1250 50  0001 C CNN
	1    9550 1250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J50
U 1 1 5C99E9ED
P 9550 1550
F 0 "J50" H 9578 1480 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9577 1435 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9550 1550 50  0001 C CNN
F 3 "~" H 9550 1550 50  0001 C CNN
	1    9550 1550
	-1   0    0    1   
$EndComp
Wire Wire Line
	9750 1250 9800 1250
Wire Wire Line
	9750 1450 9800 1450
Wire Wire Line
	9750 1550 9800 1550
Wire Wire Line
	9800 1550 9800 1450
Connection ~ 9800 1450
Wire Wire Line
	9800 1450 9850 1450
Wire Wire Line
	9750 1150 9800 1150
Wire Wire Line
	9800 1150 9800 1250
Connection ~ 9800 1250
Wire Wire Line
	9800 1250 9850 1250
$Comp
L Connector:Conn_01x02_Female J51
U 1 1 5CA0DA45
P 10750 1250
F 0 "J51" H 10778 1180 50  0000 L CNN
F 1 "Conn_01x02_Female" H 10777 1135 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 10750 1250 50  0001 C CNN
F 3 "~" H 10750 1250 50  0001 C CNN
	1    10750 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 1350 10500 1350
Wire Wire Line
	10550 1250 10500 1250
Wire Wire Line
	10500 1250 10500 1350
Connection ~ 10500 1350
Wire Wire Line
	10500 1350 10450 1350
$Comp
L Connector:Conn_01x02_Female J52
U 1 1 5CA253A2
P 11000 1900
F 0 "J52" H 11028 1830 50  0000 L CNN
F 1 "Conn_01x02_Female" H 11027 1785 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 11000 1900 50  0001 C CNN
F 3 "~" H 11000 1900 50  0001 C CNN
	1    11000 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 2000 10750 2000
Wire Wire Line
	10800 1900 10750 1900
Wire Wire Line
	10750 1900 10750 2000
Connection ~ 10750 2000
Wire Wire Line
	10750 2000 10700 2000
$Comp
L Connector:Conn_01x02_Female J48
U 1 1 5CA3D9C5
P 9400 2000
F 0 "J48" H 9428 1930 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9427 1885 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9400 2000 50  0001 C CNN
F 3 "~" H 9400 2000 50  0001 C CNN
	1    9400 2000
	-1   0    0    1   
$EndComp
Wire Wire Line
	9600 2000 9650 2000
Wire Wire Line
	9600 1900 9650 1900
Wire Wire Line
	9650 1900 9650 2000
Connection ~ 9650 2000
Wire Wire Line
	9650 2000 9700 2000
$Comp
L 74xx:74HC00 U5
U 1 1 5CABB39E
P 8400 3000
F 0 "U5" H 8400 3325 50  0000 C CNN
F 1 "74HC00" H 8400 3234 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8400 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8400 3000 50  0001 C CNN
	1    8400 3000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U5
U 2 1 5CABB3A4
P 8400 3550
F 0 "U5" H 8400 3875 50  0000 C CNN
F 1 "74HC00" H 8400 3784 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8400 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8400 3550 50  0001 C CNN
	2    8400 3550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U5
U 3 1 5CABB3AA
P 8400 4100
F 0 "U5" H 8400 4425 50  0000 C CNN
F 1 "74HC00" H 8400 4334 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8400 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 8400 4100 50  0001 C CNN
	3    8400 4100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U5
U 4 1 5CABB3B0
P 10150 3450
F 0 "U5" H 10150 3775 50  0000 C CNN
F 1 "74HC00" H 10150 3684 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 10150 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 10150 3450 50  0001 C CNN
	4    10150 3450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U5
U 5 1 5CABB3B6
P 10200 4100
F 0 "U5" V 9833 4100 50  0000 C CNN
F 1 "74HC00" V 9924 4100 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 10200 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 10200 4100 50  0001 C CNN
	5    10200 4100
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J59
U 1 1 5CABB3BC
P 9000 2900
F 0 "J59" H 9028 2830 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9027 2785 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9000 2900 50  0001 C CNN
F 3 "~" H 9000 2900 50  0001 C CNN
	1    9000 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J60
U 1 1 5CABB3C2
P 9000 3450
F 0 "J60" H 9028 3380 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9027 3335 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9000 3450 50  0001 C CNN
F 3 "~" H 9000 3450 50  0001 C CNN
	1    9000 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J61
U 1 1 5CABB3C8
P 9000 4000
F 0 "J61" H 9028 3930 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9027 3885 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9000 4000 50  0001 C CNN
F 3 "~" H 9000 4000 50  0001 C CNN
	1    9000 4000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J53
U 1 1 5CABB3CE
P 7800 2900
F 0 "J53" H 7828 2830 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 2785 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 2900 50  0001 C CNN
F 3 "~" H 7800 2900 50  0001 C CNN
	1    7800 2900
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J54
U 1 1 5CABB3D4
P 7800 3200
F 0 "J54" H 7828 3130 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 3085 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 3200 50  0001 C CNN
F 3 "~" H 7800 3200 50  0001 C CNN
	1    7800 3200
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J55
U 1 1 5CABB3DA
P 7800 3450
F 0 "J55" H 7828 3380 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 3335 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 3450 50  0001 C CNN
F 3 "~" H 7800 3450 50  0001 C CNN
	1    7800 3450
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J56
U 1 1 5CABB3E0
P 7800 3750
F 0 "J56" H 7828 3680 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 3635 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 3750 50  0001 C CNN
F 3 "~" H 7800 3750 50  0001 C CNN
	1    7800 3750
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J57
U 1 1 5CABB3E6
P 7800 4000
F 0 "J57" H 7828 3930 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 3885 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 4000 50  0001 C CNN
F 3 "~" H 7800 4000 50  0001 C CNN
	1    7800 4000
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J58
U 1 1 5CABB3EC
P 7800 4300
F 0 "J58" H 7828 4230 50  0000 L CNN
F 1 "Conn_01x02_Female" H 7827 4185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 7800 4300 50  0001 C CNN
F 3 "~" H 7800 4300 50  0001 C CNN
	1    7800 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	8000 2800 8050 2800
Wire Wire Line
	8050 2800 8050 2900
Wire Wire Line
	8050 2900 8100 2900
Wire Wire Line
	8050 2900 8000 2900
Connection ~ 8050 2900
Wire Wire Line
	8000 3100 8050 3100
Wire Wire Line
	8000 3200 8050 3200
Wire Wire Line
	8050 3200 8050 3100
Connection ~ 8050 3100
Wire Wire Line
	8050 3100 8100 3100
Wire Wire Line
	8000 3450 8050 3450
Wire Wire Line
	8000 3350 8050 3350
Wire Wire Line
	8050 3350 8050 3450
Connection ~ 8050 3450
Wire Wire Line
	8050 3450 8100 3450
Wire Wire Line
	8000 3650 8050 3650
Wire Wire Line
	8000 3750 8050 3750
Wire Wire Line
	8050 3750 8050 3650
Connection ~ 8050 3650
Wire Wire Line
	8050 3650 8100 3650
Wire Wire Line
	8000 3900 8050 3900
Wire Wire Line
	8050 3900 8050 4000
Wire Wire Line
	8050 4000 8100 4000
Wire Wire Line
	8050 4000 8000 4000
Connection ~ 8050 4000
Wire Wire Line
	8000 4200 8050 4200
Wire Wire Line
	8050 4200 8050 4300
Wire Wire Line
	8050 4300 8000 4300
Connection ~ 8050 4200
Wire Wire Line
	8050 4200 8100 4200
Wire Wire Line
	8700 4100 8750 4100
Wire Wire Line
	8800 4000 8750 4000
Wire Wire Line
	8750 4000 8750 4100
Connection ~ 8750 4100
Wire Wire Line
	8750 4100 8800 4100
Wire Wire Line
	8800 3550 8750 3550
Wire Wire Line
	8800 3450 8750 3450
Wire Wire Line
	8750 3450 8750 3550
Connection ~ 8750 3550
Wire Wire Line
	8750 3550 8700 3550
Wire Wire Line
	8800 3000 8750 3000
Wire Wire Line
	8800 2900 8750 2900
Wire Wire Line
	8750 2900 8750 3000
Connection ~ 8750 3000
Wire Wire Line
	8750 3000 8700 3000
$Comp
L Connector:Conn_01x02_Female J63
U 1 1 5CABB41F
P 9550 3350
F 0 "J63" H 9578 3280 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9577 3235 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9550 3350 50  0001 C CNN
F 3 "~" H 9550 3350 50  0001 C CNN
	1    9550 3350
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J64
U 1 1 5CABB425
P 9550 3650
F 0 "J64" H 9578 3580 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9577 3535 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9550 3650 50  0001 C CNN
F 3 "~" H 9550 3650 50  0001 C CNN
	1    9550 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	9750 3350 9800 3350
Wire Wire Line
	9750 3550 9800 3550
Wire Wire Line
	9750 3650 9800 3650
Wire Wire Line
	9800 3650 9800 3550
Connection ~ 9800 3550
Wire Wire Line
	9800 3550 9850 3550
Wire Wire Line
	9750 3250 9800 3250
Wire Wire Line
	9800 3250 9800 3350
Connection ~ 9800 3350
Wire Wire Line
	9800 3350 9850 3350
$Comp
L Connector:Conn_01x02_Female J65
U 1 1 5CABB435
P 10750 3350
F 0 "J65" H 10778 3280 50  0000 L CNN
F 1 "Conn_01x02_Female" H 10777 3235 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 10750 3350 50  0001 C CNN
F 3 "~" H 10750 3350 50  0001 C CNN
	1    10750 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 3450 10500 3450
Wire Wire Line
	10550 3350 10500 3350
Wire Wire Line
	10500 3350 10500 3450
Connection ~ 10500 3450
Wire Wire Line
	10500 3450 10450 3450
$Comp
L Connector:Conn_01x02_Female J66
U 1 1 5CABB440
P 11000 4000
F 0 "J66" H 11028 3930 50  0000 L CNN
F 1 "Conn_01x02_Female" H 11027 3885 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 11000 4000 50  0001 C CNN
F 3 "~" H 11000 4000 50  0001 C CNN
	1    11000 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 4100 10750 4100
Wire Wire Line
	10800 4000 10750 4000
Wire Wire Line
	10750 4000 10750 4100
Connection ~ 10750 4100
Wire Wire Line
	10750 4100 10700 4100
$Comp
L Connector:Conn_01x02_Female J62
U 1 1 5CABB44B
P 9400 4100
F 0 "J62" H 9428 4030 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9427 3985 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 9400 4100 50  0001 C CNN
F 3 "~" H 9400 4100 50  0001 C CNN
	1    9400 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	9600 4100 9650 4100
Wire Wire Line
	9600 4000 9650 4000
Wire Wire Line
	9650 4000 9650 4100
Connection ~ 9650 4100
Wire Wire Line
	9650 4100 9700 4100
$Comp
L Device:R R10
U 1 1 5CADE529
P 4950 4400
F 0 "R10" V 4743 4400 50  0000 C CNN
F 1 "0603-1206" V 4834 4400 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 4400 50  0001 C CNN
F 3 "~" H 4950 4400 50  0001 C CNN
	1    4950 4400
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J69
U 1 1 5CADE52F
P 5400 4400
F 0 "J69" H 5428 4330 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 4285 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 4400 50  0001 C CNN
F 3 "~" H 5400 4400 50  0001 C CNN
	1    5400 4400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J67
U 1 1 5CADE535
P 4500 4500
F 0 "J67" H 4394 4267 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 4266 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 4500 50  0001 C CNN
F 3 "~" H 4500 4500 50  0001 C CNN
	1    4500 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 4500 4750 4500
Wire Wire Line
	4750 4500 4750 4400
Wire Wire Line
	4750 4400 4800 4400
Wire Wire Line
	4750 4400 4700 4400
Connection ~ 4750 4400
Wire Wire Line
	5100 4400 5150 4400
Wire Wire Line
	5200 4500 5150 4500
Wire Wire Line
	5150 4500 5150 4400
Connection ~ 5150 4400
Wire Wire Line
	5150 4400 5200 4400
$Comp
L Device:R R11
U 1 1 5CADE545
P 4950 4800
F 0 "R11" V 4743 4800 50  0000 C CNN
F 1 "0603-1206" V 4834 4800 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 4880 4800 50  0001 C CNN
F 3 "~" H 4950 4800 50  0001 C CNN
	1    4950 4800
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J70
U 1 1 5CADE54B
P 5400 4800
F 0 "J70" H 5428 4730 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 4685 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 4800 50  0001 C CNN
F 3 "~" H 5400 4800 50  0001 C CNN
	1    5400 4800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J68
U 1 1 5CADE551
P 4500 4900
F 0 "J68" H 4394 4667 50  0000 C CNN
F 1 "Conn_01x02_Female" H 4394 4666 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 4500 4900 50  0001 C CNN
F 3 "~" H 4500 4900 50  0001 C CNN
	1    4500 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 4900 4750 4900
Wire Wire Line
	4750 4900 4750 4800
Wire Wire Line
	4750 4800 4800 4800
Wire Wire Line
	4750 4800 4700 4800
Connection ~ 4750 4800
Wire Wire Line
	5100 4800 5150 4800
Wire Wire Line
	5200 4900 5150 4900
Wire Wire Line
	5150 4900 5150 4800
Connection ~ 5150 4800
Wire Wire Line
	5150 4800 5200 4800
$Comp
L Connector:Conn_01x03_Female J71
U 1 1 5CB516DD
P 800 6300
F 0 "J71" V 900 6350 50  0000 L CNN
F 1 "Conn_01x03_Female" V 738 6448 50  0001 L CNN
F 2 "custom-footprints:3-pin" H 800 6300 50  0001 C CNN
F 3 "~" H 800 6300 50  0001 C CNN
	1    800  6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Female J73
U 1 1 5CB51916
P 1700 6300
F 0 "J73" V 1800 6350 50  0000 L CNN
F 1 "Conn_01x03_Female" V 1638 6448 50  0001 L CNN
F 2 "custom-footprints:3-pin" H 1700 6300 50  0001 C CNN
F 3 "~" H 1700 6300 50  0001 C CNN
	1    1700 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Female J72
U 1 1 5CB51B5E
P 1300 6300
F 0 "J72" V 1400 6350 50  0000 L CNN
F 1 "Conn_01x04_Female" V 1238 6448 50  0001 L CNN
F 2 "custom-footprints:4-pin" H 1300 6300 50  0001 C CNN
F 3 "~" H 1300 6300 50  0001 C CNN
	1    1300 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	700  6100 700  6050
Wire Wire Line
	700  6050 800  6050
Wire Wire Line
	1800 6050 1800 6100
Wire Wire Line
	800  6100 800  6050
Connection ~ 800  6050
Wire Wire Line
	800  6050 900  6050
Wire Wire Line
	900  6100 900  6050
Wire Wire Line
	1100 6100 1100 6050
Wire Wire Line
	1100 6050 1200 6050
Wire Wire Line
	1200 6100 1200 6050
Connection ~ 1200 6050
Wire Wire Line
	1200 6050 1300 6050
Wire Wire Line
	1300 6100 1300 6050
Connection ~ 1300 6050
Wire Wire Line
	1300 6050 1400 6050
Wire Wire Line
	1400 6100 1400 6050
Wire Wire Line
	1600 6100 1600 6050
Wire Wire Line
	1600 6050 1700 6050
Wire Wire Line
	1700 6100 1700 6050
Connection ~ 1700 6050
Wire Wire Line
	1700 6050 1800 6050
$Comp
L Connector:Conn_01x03_Female J74
U 1 1 5CD153D6
P 800 6750
F 0 "J74" V 900 6800 50  0000 L CNN
F 1 "Conn_01x03_Female" V 738 6898 50  0001 L CNN
F 2 "custom-footprints:3-pin" H 800 6750 50  0001 C CNN
F 3 "~" H 800 6750 50  0001 C CNN
	1    800  6750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Female J80
U 1 1 5CD153DC
P 1700 6750
F 0 "J80" V 1800 6800 50  0000 L CNN
F 1 "Conn_01x03_Female" V 1638 6898 50  0001 L CNN
F 2 "custom-footprints:3-pin" H 1700 6750 50  0001 C CNN
F 3 "~" H 1700 6750 50  0001 C CNN
	1    1700 6750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Female J77
U 1 1 5CD153E2
P 1300 6750
F 0 "J77" V 1400 6800 50  0000 L CNN
F 1 "Conn_01x04_Female" V 1238 6898 50  0001 L CNN
F 2 "custom-footprints:4-pin" H 1300 6750 50  0001 C CNN
F 3 "~" H 1300 6750 50  0001 C CNN
	1    1300 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	700  6550 700  6500
Wire Wire Line
	700  6500 800  6500
Wire Wire Line
	1800 6500 1800 6550
Wire Wire Line
	800  6550 800  6500
Connection ~ 800  6500
Wire Wire Line
	800  6500 900  6500
Wire Wire Line
	900  6550 900  6500
Wire Wire Line
	1100 6550 1100 6500
Wire Wire Line
	1100 6500 1200 6500
Wire Wire Line
	1200 6550 1200 6500
Connection ~ 1200 6500
Wire Wire Line
	1200 6500 1300 6500
Wire Wire Line
	1300 6550 1300 6500
Connection ~ 1300 6500
Wire Wire Line
	1300 6500 1400 6500
Wire Wire Line
	1400 6550 1400 6500
Wire Wire Line
	1600 6550 1600 6500
Wire Wire Line
	1600 6500 1700 6500
Wire Wire Line
	1700 6550 1700 6500
Connection ~ 1700 6500
Wire Wire Line
	1700 6500 1800 6500
$Comp
L Connector:Conn_01x03_Female J75
U 1 1 5CD4479A
P 800 7200
F 0 "J75" V 900 7250 50  0000 L CNN
F 1 "Conn_01x03_Female" V 738 7348 50  0001 L CNN
F 2 "custom-footprints:3-pin" H 800 7200 50  0001 C CNN
F 3 "~" H 800 7200 50  0001 C CNN
	1    800  7200
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Female J81
U 1 1 5CD447A0
P 1700 7200
F 0 "J81" V 1800 7250 50  0000 L CNN
F 1 "Conn_01x03_Female" V 1638 7348 50  0001 L CNN
F 2 "custom-footprints:3-pin" H 1700 7200 50  0001 C CNN
F 3 "~" H 1700 7200 50  0001 C CNN
	1    1700 7200
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Female J78
U 1 1 5CD447A6
P 1300 7200
F 0 "J78" V 1400 7250 50  0000 L CNN
F 1 "Conn_01x04_Female" V 1238 7348 50  0001 L CNN
F 2 "custom-footprints:4-pin" H 1300 7200 50  0001 C CNN
F 3 "~" H 1300 7200 50  0001 C CNN
	1    1300 7200
	0    1    1    0   
$EndComp
Wire Wire Line
	700  7000 700  6950
Wire Wire Line
	700  6950 800  6950
Wire Wire Line
	1800 6950 1800 7000
Wire Wire Line
	800  7000 800  6950
Connection ~ 800  6950
Wire Wire Line
	800  6950 900  6950
Wire Wire Line
	900  7000 900  6950
Wire Wire Line
	1100 7000 1100 6950
Wire Wire Line
	1100 6950 1200 6950
Wire Wire Line
	1200 7000 1200 6950
Connection ~ 1200 6950
Wire Wire Line
	1200 6950 1300 6950
Wire Wire Line
	1300 7000 1300 6950
Connection ~ 1300 6950
Wire Wire Line
	1300 6950 1400 6950
Wire Wire Line
	1400 7000 1400 6950
Wire Wire Line
	1600 7000 1600 6950
Wire Wire Line
	1600 6950 1700 6950
Wire Wire Line
	1700 7000 1700 6950
Connection ~ 1700 6950
Wire Wire Line
	1700 6950 1800 6950
$Comp
L Connector:Conn_01x03_Female J76
U 1 1 5CD447C1
P 800 7650
F 0 "J76" V 900 7700 50  0000 L CNN
F 1 "Conn_01x03_Female" V 738 7798 50  0001 L CNN
F 2 "custom-footprints:3-pin" H 800 7650 50  0001 C CNN
F 3 "~" H 800 7650 50  0001 C CNN
	1    800  7650
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Female J82
U 1 1 5CD447C7
P 1700 7650
F 0 "J82" V 1800 7700 50  0000 L CNN
F 1 "Conn_01x03_Female" V 1638 7798 50  0001 L CNN
F 2 "custom-footprints:3-pin" H 1700 7650 50  0001 C CNN
F 3 "~" H 1700 7650 50  0001 C CNN
	1    1700 7650
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Female J79
U 1 1 5CD447CD
P 1300 7650
F 0 "J79" V 1400 7700 50  0000 L CNN
F 1 "Conn_01x04_Female" V 1238 7798 50  0001 L CNN
F 2 "custom-footprints:4-pin" H 1300 7650 50  0001 C CNN
F 3 "~" H 1300 7650 50  0001 C CNN
	1    1300 7650
	0    1    1    0   
$EndComp
Wire Wire Line
	700  7450 700  7400
Wire Wire Line
	700  7400 800  7400
Wire Wire Line
	1800 7400 1800 7450
Wire Wire Line
	800  7450 800  7400
Connection ~ 800  7400
Wire Wire Line
	800  7400 900  7400
Wire Wire Line
	900  7450 900  7400
Wire Wire Line
	1100 7450 1100 7400
Wire Wire Line
	1100 7400 1200 7400
Wire Wire Line
	1200 7450 1200 7400
Connection ~ 1200 7400
Wire Wire Line
	1200 7400 1300 7400
Wire Wire Line
	1300 7450 1300 7400
Connection ~ 1300 7400
Wire Wire Line
	1300 7400 1400 7400
Wire Wire Line
	1400 7450 1400 7400
Wire Wire Line
	1600 7450 1600 7400
Wire Wire Line
	1600 7400 1700 7400
Wire Wire Line
	1700 7450 1700 7400
Connection ~ 1700 7400
Wire Wire Line
	1700 7400 1800 7400
$Comp
L Connector:Conn_01x02_Female J83
U 1 1 5CDB7AE8
P 4650 5400
F 0 "J83" V 4750 5350 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4677 5285 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 4650 5400 50  0001 C CNN
F 3 "~" H 4650 5400 50  0001 C CNN
	1    4650 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 5200 4550 5150
Wire Wire Line
	4550 5150 4650 5150
Wire Wire Line
	4650 5150 4650 5200
$Comp
L Connector:Conn_01x02_Female J84
U 1 1 5CDF1EE6
P 4900 5400
F 0 "J84" V 5000 5350 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4927 5285 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 4900 5400 50  0001 C CNN
F 3 "~" H 4900 5400 50  0001 C CNN
	1    4900 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 5200 4800 5150
Wire Wire Line
	4800 5150 4900 5150
Wire Wire Line
	4900 5150 4900 5200
$Comp
L Connector:Conn_01x02_Female J85
U 1 1 5CE2C5D6
P 5150 5400
F 0 "J85" V 5250 5350 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5177 5285 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5150 5400 50  0001 C CNN
F 3 "~" H 5150 5400 50  0001 C CNN
	1    5150 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 5200 5050 5150
Wire Wire Line
	5050 5150 5150 5150
Wire Wire Line
	5150 5150 5150 5200
$Comp
L Connector:Conn_01x02_Female J86
U 1 1 5CE2C5DF
P 5400 5400
F 0 "J86" V 5500 5350 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 5285 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 5400 50  0001 C CNN
F 3 "~" H 5400 5400 50  0001 C CNN
	1    5400 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	5300 5200 5300 5150
Wire Wire Line
	5300 5150 5400 5150
Wire Wire Line
	5400 5150 5400 5200
$Comp
L Connector:Conn_01x01_Female J87
U 1 1 5CE69926
P 8250 5250
F 0 "J87" V 8350 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8188 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8250 5250 50  0001 C CNN
F 3 "~" H 8250 5250 50  0001 C CNN
	1    8250 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J88
U 1 1 5CE69E68
P 8000 5250
F 0 "J88" V 8100 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 7938 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8000 5250 50  0001 C CNN
F 3 "~" H 8000 5250 50  0001 C CNN
	1    8000 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J89
U 1 1 5CE6A020
P 7750 5250
F 0 "J89" V 7850 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 7688 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 7750 5250 50  0001 C CNN
F 3 "~" H 7750 5250 50  0001 C CNN
	1    7750 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J90
U 1 1 5CE6A026
P 7500 5250
F 0 "J90" V 7600 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 7438 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 7500 5250 50  0001 C CNN
F 3 "~" H 7500 5250 50  0001 C CNN
	1    7500 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J91
U 1 1 5CEA735A
P 7500 5750
F 0 "J91" V 7600 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 7438 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 7500 5750 50  0001 C CNN
F 3 "~" H 7500 5750 50  0001 C CNN
	1    7500 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J92
U 1 1 5CEA7360
P 7750 5750
F 0 "J92" V 7850 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 7688 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 7750 5750 50  0001 C CNN
F 3 "~" H 7750 5750 50  0001 C CNN
	1    7750 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J93
U 1 1 5CEA7366
P 8000 5750
F 0 "J93" V 8100 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 7938 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8000 5750 50  0001 C CNN
F 3 "~" H 8000 5750 50  0001 C CNN
	1    8000 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J94
U 1 1 5CEA736C
P 8250 5750
F 0 "J94" V 8350 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8188 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8250 5750 50  0001 C CNN
F 3 "~" H 8250 5750 50  0001 C CNN
	1    8250 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	7500 5450 7500 5500
Wire Wire Line
	7750 5450 7750 5500
Wire Wire Line
	8000 5450 8000 5500
Wire Wire Line
	8250 5450 8250 5500
$Comp
L Connector:Conn_01x01_Female J101
U 1 1 5D102C8C
P 9250 5250
F 0 "J101" V 9350 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9188 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9250 5250 50  0001 C CNN
F 3 "~" H 9250 5250 50  0001 C CNN
	1    9250 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J99
U 1 1 5D102C92
P 9000 5250
F 0 "J99" V 9100 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8938 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9000 5250 50  0001 C CNN
F 3 "~" H 9000 5250 50  0001 C CNN
	1    9000 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J97
U 1 1 5D102C98
P 8750 5250
F 0 "J97" V 8850 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8688 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8750 5250 50  0001 C CNN
F 3 "~" H 8750 5250 50  0001 C CNN
	1    8750 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J95
U 1 1 5D102C9E
P 8500 5250
F 0 "J95" V 8600 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8438 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8500 5250 50  0001 C CNN
F 3 "~" H 8500 5250 50  0001 C CNN
	1    8500 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J96
U 1 1 5D102CA4
P 8500 5750
F 0 "J96" V 8600 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8438 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8500 5750 50  0001 C CNN
F 3 "~" H 8500 5750 50  0001 C CNN
	1    8500 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J98
U 1 1 5D102CAA
P 8750 5750
F 0 "J98" V 8850 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8688 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8750 5750 50  0001 C CNN
F 3 "~" H 8750 5750 50  0001 C CNN
	1    8750 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J100
U 1 1 5D102CB0
P 9000 5750
F 0 "J100" V 9100 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8938 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9000 5750 50  0001 C CNN
F 3 "~" H 9000 5750 50  0001 C CNN
	1    9000 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J102
U 1 1 5D102CB6
P 9250 5750
F 0 "J102" V 9350 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9188 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9250 5750 50  0001 C CNN
F 3 "~" H 9250 5750 50  0001 C CNN
	1    9250 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	8500 5450 8500 5500
Wire Wire Line
	8750 5450 8750 5500
Wire Wire Line
	9000 5450 9000 5500
Wire Wire Line
	9250 5450 9250 5500
$Comp
L Connector:Conn_01x01_Female J109
U 1 1 5D140A3E
P 10250 5250
F 0 "J109" V 10350 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10188 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10250 5250 50  0001 C CNN
F 3 "~" H 10250 5250 50  0001 C CNN
	1    10250 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J107
U 1 1 5D140A44
P 10000 5250
F 0 "J107" V 10100 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9938 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10000 5250 50  0001 C CNN
F 3 "~" H 10000 5250 50  0001 C CNN
	1    10000 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J105
U 1 1 5D140A4A
P 9750 5250
F 0 "J105" V 9850 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9688 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9750 5250 50  0001 C CNN
F 3 "~" H 9750 5250 50  0001 C CNN
	1    9750 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J103
U 1 1 5D140A50
P 9500 5250
F 0 "J103" V 9600 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9438 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9500 5250 50  0001 C CNN
F 3 "~" H 9500 5250 50  0001 C CNN
	1    9500 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J104
U 1 1 5D140A56
P 9500 5750
F 0 "J104" V 9600 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9438 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9500 5750 50  0001 C CNN
F 3 "~" H 9500 5750 50  0001 C CNN
	1    9500 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J106
U 1 1 5D140A5C
P 9750 5750
F 0 "J106" V 9850 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9688 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9750 5750 50  0001 C CNN
F 3 "~" H 9750 5750 50  0001 C CNN
	1    9750 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J108
U 1 1 5D140A62
P 10000 5750
F 0 "J108" V 10100 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9938 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10000 5750 50  0001 C CNN
F 3 "~" H 10000 5750 50  0001 C CNN
	1    10000 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J110
U 1 1 5D140A68
P 10250 5750
F 0 "J110" V 10350 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10188 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10250 5750 50  0001 C CNN
F 3 "~" H 10250 5750 50  0001 C CNN
	1    10250 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	9500 5450 9500 5500
Wire Wire Line
	9750 5450 9750 5500
Wire Wire Line
	10000 5450 10000 5500
Wire Wire Line
	10250 5450 10250 5500
$Comp
L Connector:Conn_01x01_Female J115
U 1 1 5D1BEB22
P 11000 5250
F 0 "J115" V 11100 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10938 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 11000 5250 50  0001 C CNN
F 3 "~" H 11000 5250 50  0001 C CNN
	1    11000 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J113
U 1 1 5D1BEB28
P 10750 5250
F 0 "J113" V 10850 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10688 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10750 5250 50  0001 C CNN
F 3 "~" H 10750 5250 50  0001 C CNN
	1    10750 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J111
U 1 1 5D1BEB2E
P 10500 5250
F 0 "J111" V 10600 5250 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10438 5298 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10500 5250 50  0001 C CNN
F 3 "~" H 10500 5250 50  0001 C CNN
	1    10500 5250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J112
U 1 1 5D1BEB34
P 10500 5750
F 0 "J112" V 10600 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10438 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10500 5750 50  0001 C CNN
F 3 "~" H 10500 5750 50  0001 C CNN
	1    10500 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J114
U 1 1 5D1BEB3A
P 10750 5750
F 0 "J114" V 10850 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10688 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10750 5750 50  0001 C CNN
F 3 "~" H 10750 5750 50  0001 C CNN
	1    10750 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J116
U 1 1 5D1BEB40
P 11000 5750
F 0 "J116" V 11100 5750 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10938 5798 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 11000 5750 50  0001 C CNN
F 3 "~" H 11000 5750 50  0001 C CNN
	1    11000 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	10500 5450 10500 5500
Wire Wire Line
	10750 5450 10750 5500
Wire Wire Line
	11000 5450 11000 5500
$Comp
L Connector:Conn_01x01_Female J118
U 1 1 5D2BBE4B
P 7400 6300
F 0 "J118" V 7500 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 7338 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 7400 6300 50  0001 C CNN
F 3 "~" H 7400 6300 50  0001 C CNN
	1    7400 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J120
U 1 1 5D2BBE51
P 7650 6300
F 0 "J120" V 7750 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 7588 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 7650 6300 50  0001 C CNN
F 3 "~" H 7650 6300 50  0001 C CNN
	1    7650 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J122
U 1 1 5D2BBE57
P 7900 6300
F 0 "J122" V 8000 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 7838 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 7900 6300 50  0001 C CNN
F 3 "~" H 7900 6300 50  0001 C CNN
	1    7900 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J124
U 1 1 5D2BBE5D
P 8150 6300
F 0 "J124" V 8250 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8088 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8150 6300 50  0001 C CNN
F 3 "~" H 8150 6300 50  0001 C CNN
	1    8150 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J126
U 1 1 5D2BBE7F
P 8400 6300
F 0 "J126" V 8500 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8338 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8400 6300 50  0001 C CNN
F 3 "~" H 8400 6300 50  0001 C CNN
	1    8400 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J128
U 1 1 5D2BBE85
P 8650 6300
F 0 "J128" V 8750 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8588 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8650 6300 50  0001 C CNN
F 3 "~" H 8650 6300 50  0001 C CNN
	1    8650 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J130
U 1 1 5D2BBE8B
P 8900 6300
F 0 "J130" V 9000 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 8838 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 8900 6300 50  0001 C CNN
F 3 "~" H 8900 6300 50  0001 C CNN
	1    8900 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J132
U 1 1 5D2BBE91
P 9150 6300
F 0 "J132" V 9250 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9088 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9150 6300 50  0001 C CNN
F 3 "~" H 9150 6300 50  0001 C CNN
	1    9150 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J134
U 1 1 5D2BBEB3
P 9400 6300
F 0 "J134" V 9500 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9338 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9400 6300 50  0001 C CNN
F 3 "~" H 9400 6300 50  0001 C CNN
	1    9400 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J136
U 1 1 5D2BBEB9
P 9650 6300
F 0 "J136" V 9750 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9588 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9650 6300 50  0001 C CNN
F 3 "~" H 9650 6300 50  0001 C CNN
	1    9650 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J138
U 1 1 5D2BBEBF
P 9900 6300
F 0 "J138" V 10000 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 9838 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 9900 6300 50  0001 C CNN
F 3 "~" H 9900 6300 50  0001 C CNN
	1    9900 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J140
U 1 1 5D2BBEC5
P 10150 6300
F 0 "J140" V 10250 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10088 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10150 6300 50  0001 C CNN
F 3 "~" H 10150 6300 50  0001 C CNN
	1    10150 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J142
U 1 1 5D2BBEE1
P 10400 6300
F 0 "J142" V 10500 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10338 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10400 6300 50  0001 C CNN
F 3 "~" H 10400 6300 50  0001 C CNN
	1    10400 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J144
U 1 1 5D2BBEE7
P 10650 6300
F 0 "J144" V 10750 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10588 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10650 6300 50  0001 C CNN
F 3 "~" H 10650 6300 50  0001 C CNN
	1    10650 6300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J146
U 1 1 5D2BBEED
P 10900 6300
F 0 "J146" V 11000 6300 50  0000 L CNN
F 1 "Conn_01x01_Female" V 10838 6348 50  0001 L CNN
F 2 "custom-footprints:1-pin" H 10900 6300 50  0001 C CNN
F 3 "~" H 10900 6300 50  0001 C CNN
	1    10900 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 5500 7500 5500
Wire Wire Line
	7400 5500 7400 6100
Connection ~ 7500 5500
Wire Wire Line
	7500 5500 7500 5550
Wire Wire Line
	7650 5500 7750 5500
Wire Wire Line
	7650 5500 7650 6100
Connection ~ 7750 5500
Wire Wire Line
	7750 5500 7750 5550
Wire Wire Line
	7900 5500 8000 5500
Wire Wire Line
	7900 5500 7900 6100
Connection ~ 8000 5500
Wire Wire Line
	8000 5500 8000 5550
Wire Wire Line
	8150 5500 8250 5500
Wire Wire Line
	8150 5500 8150 6100
Connection ~ 8250 5500
Wire Wire Line
	8250 5500 8250 5550
Wire Wire Line
	8400 5500 8500 5500
Wire Wire Line
	8400 5500 8400 6100
Connection ~ 8500 5500
Wire Wire Line
	8500 5500 8500 5550
Wire Wire Line
	8650 5500 8750 5500
Wire Wire Line
	8650 5500 8650 6100
Connection ~ 8750 5500
Wire Wire Line
	8750 5500 8750 5550
Wire Wire Line
	8900 5500 9000 5500
Wire Wire Line
	8900 5500 8900 6100
Connection ~ 9000 5500
Wire Wire Line
	9000 5500 9000 5550
Wire Wire Line
	9150 5500 9250 5500
Wire Wire Line
	9150 5500 9150 6100
Connection ~ 9250 5500
Wire Wire Line
	9250 5500 9250 5550
Wire Wire Line
	9400 5500 9500 5500
Wire Wire Line
	9400 5500 9400 6100
Connection ~ 9500 5500
Wire Wire Line
	9500 5500 9500 5550
Wire Wire Line
	9650 5500 9750 5500
Wire Wire Line
	9650 5500 9650 6100
Connection ~ 9750 5500
Wire Wire Line
	9750 5500 9750 5550
Wire Wire Line
	9900 5500 10000 5500
Wire Wire Line
	9900 5500 9900 6100
Connection ~ 10000 5500
Wire Wire Line
	10000 5500 10000 5550
Wire Wire Line
	10150 5500 10250 5500
Wire Wire Line
	10150 5500 10150 6100
Connection ~ 10250 5500
Wire Wire Line
	10250 5500 10250 5550
Wire Wire Line
	10400 5500 10500 5500
Wire Wire Line
	10400 5500 10400 6100
Connection ~ 10500 5500
Wire Wire Line
	10500 5500 10500 5550
Wire Wire Line
	10650 5500 10750 5500
Wire Wire Line
	10650 5500 10650 6100
Connection ~ 10750 5500
Wire Wire Line
	10750 5500 10750 5550
Wire Wire Line
	10900 5500 11000 5500
Wire Wire Line
	10900 5500 10900 6100
Connection ~ 11000 5500
Wire Wire Line
	11000 5500 11000 5550
$Comp
L Connector:Conn_01x02_Female J117
U 1 1 5C833103
P 4650 5850
F 0 "J117" V 4750 5800 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4677 5735 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 4650 5850 50  0001 C CNN
F 3 "~" H 4650 5850 50  0001 C CNN
	1    4650 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 5650 4550 5600
Wire Wire Line
	4550 5600 4650 5600
Wire Wire Line
	4650 5600 4650 5650
$Comp
L Connector:Conn_01x02_Female J123
U 1 1 5C83310C
P 4900 5850
F 0 "J123" V 5000 5800 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4927 5735 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 4900 5850 50  0001 C CNN
F 3 "~" H 4900 5850 50  0001 C CNN
	1    4900 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 5650 4800 5600
Wire Wire Line
	4800 5600 4900 5600
Wire Wire Line
	4900 5600 4900 5650
$Comp
L Connector:Conn_01x02_Female J129
U 1 1 5C833115
P 5150 5850
F 0 "J129" V 5250 5800 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5177 5735 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5150 5850 50  0001 C CNN
F 3 "~" H 5150 5850 50  0001 C CNN
	1    5150 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 5650 5050 5600
Wire Wire Line
	5050 5600 5150 5600
Wire Wire Line
	5150 5600 5150 5650
$Comp
L Connector:Conn_01x02_Female J135
U 1 1 5C83311E
P 5400 5850
F 0 "J135" V 5500 5800 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 5735 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 5850 50  0001 C CNN
F 3 "~" H 5400 5850 50  0001 C CNN
	1    5400 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	5300 5650 5300 5600
Wire Wire Line
	5300 5600 5400 5600
Wire Wire Line
	5400 5600 5400 5650
$Comp
L Connector:Conn_01x02_Female J119
U 1 1 5C87DC74
P 4650 6300
F 0 "J119" V 4750 6250 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4677 6185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 4650 6300 50  0001 C CNN
F 3 "~" H 4650 6300 50  0001 C CNN
	1    4650 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 6100 4550 6050
Wire Wire Line
	4550 6050 4650 6050
Wire Wire Line
	4650 6050 4650 6100
$Comp
L Connector:Conn_01x02_Female J125
U 1 1 5C87DC7D
P 4900 6300
F 0 "J125" V 5000 6250 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4927 6185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 4900 6300 50  0001 C CNN
F 3 "~" H 4900 6300 50  0001 C CNN
	1    4900 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 6100 4800 6050
Wire Wire Line
	4800 6050 4900 6050
Wire Wire Line
	4900 6050 4900 6100
$Comp
L Connector:Conn_01x02_Female J131
U 1 1 5C87DC86
P 5150 6300
F 0 "J131" V 5250 6250 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5177 6185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5150 6300 50  0001 C CNN
F 3 "~" H 5150 6300 50  0001 C CNN
	1    5150 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 6100 5050 6050
Wire Wire Line
	5050 6050 5150 6050
Wire Wire Line
	5150 6050 5150 6100
$Comp
L Connector:Conn_01x02_Female J137
U 1 1 5C87DC8F
P 5400 6300
F 0 "J137" V 5500 6250 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5427 6185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5400 6300 50  0001 C CNN
F 3 "~" H 5400 6300 50  0001 C CNN
	1    5400 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	5300 6100 5300 6050
Wire Wire Line
	5300 6050 5400 6050
Wire Wire Line
	5400 6050 5400 6100
$Comp
L Connector:Conn_01x02_Female J121
U 1 1 5C87DC98
P 4650 6750
F 0 "J121" V 4750 6700 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4677 6635 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 4650 6750 50  0001 C CNN
F 3 "~" H 4650 6750 50  0001 C CNN
	1    4650 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 6550 4550 6500
Wire Wire Line
	4550 6500 4650 6500
Wire Wire Line
	4650 6500 4650 6550
$Comp
L Connector:Conn_01x02_Female J127
U 1 1 5C87DCA1
P 4900 6750
F 0 "J127" V 5000 6700 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4927 6635 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 4900 6750 50  0001 C CNN
F 3 "~" H 4900 6750 50  0001 C CNN
	1    4900 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 6550 4800 6500
Wire Wire Line
	4800 6500 4900 6500
Wire Wire Line
	4900 6500 4900 6550
$Comp
L Connector:Conn_01x02_Female J133
U 1 1 5C87DCAA
P 5150 6750
F 0 "J133" V 5250 6700 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5177 6635 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 5150 6750 50  0001 C CNN
F 3 "~" H 5150 6750 50  0001 C CNN
	1    5150 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 6550 5050 6500
Wire Wire Line
	5050 6500 5150 6500
Wire Wire Line
	5150 6500 5150 6550
$Comp
L Device:R R12
U 1 1 5C92258F
P 6150 900
F 0 "R12" V 5943 900 50  0000 C CNN
F 1 "0603-1206" V 6034 900 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 6080 900 50  0001 C CNN
F 3 "~" H 6150 900 50  0001 C CNN
	1    6150 900 
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J150
U 1 1 5C922595
P 6600 900
F 0 "J150" H 6628 830 50  0000 L CNN
F 1 "Conn_01x02_Female" H 6627 785 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 6600 900 50  0001 C CNN
F 3 "~" H 6600 900 50  0001 C CNN
	1    6600 900 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J139
U 1 1 5C92259B
P 5700 1000
F 0 "J139" H 5594 767 50  0000 C CNN
F 1 "Conn_01x02_Female" H 5594 766 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 5700 1000 50  0001 C CNN
F 3 "~" H 5700 1000 50  0001 C CNN
	1    5700 1000
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 1000 5950 1000
Wire Wire Line
	5950 1000 5950 900 
Wire Wire Line
	5950 900  6000 900 
Wire Wire Line
	5950 900  5900 900 
Connection ~ 5950 900 
Wire Wire Line
	6300 900  6350 900 
Wire Wire Line
	6400 1000 6350 1000
Wire Wire Line
	6350 1000 6350 900 
Connection ~ 6350 900 
Wire Wire Line
	6350 900  6400 900 
$Comp
L Device:R R13
U 1 1 5C9225AB
P 6150 1300
F 0 "R13" V 5943 1300 50  0000 C CNN
F 1 "0603-1206" V 6034 1300 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 6080 1300 50  0001 C CNN
F 3 "~" H 6150 1300 50  0001 C CNN
	1    6150 1300
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J151
U 1 1 5C9225B1
P 6600 1300
F 0 "J151" H 6628 1230 50  0000 L CNN
F 1 "Conn_01x02_Female" H 6627 1185 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 6600 1300 50  0001 C CNN
F 3 "~" H 6600 1300 50  0001 C CNN
	1    6600 1300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J141
U 1 1 5C9225B7
P 5700 1400
F 0 "J141" H 5594 1167 50  0000 C CNN
F 1 "Conn_01x02_Female" H 5594 1166 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 5700 1400 50  0001 C CNN
F 3 "~" H 5700 1400 50  0001 C CNN
	1    5700 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 1400 5950 1400
Wire Wire Line
	5950 1400 5950 1300
Wire Wire Line
	5950 1300 6000 1300
Wire Wire Line
	5950 1300 5900 1300
Connection ~ 5950 1300
Wire Wire Line
	6300 1300 6350 1300
Wire Wire Line
	6400 1400 6350 1400
Wire Wire Line
	6350 1400 6350 1300
Connection ~ 6350 1300
Wire Wire Line
	6350 1300 6400 1300
$Comp
L Device:R R14
U 1 1 5C9225C7
P 6150 1650
F 0 "R14" V 5943 1650 50  0000 C CNN
F 1 "0603-1206" V 6034 1650 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 6080 1650 50  0001 C CNN
F 3 "~" H 6150 1650 50  0001 C CNN
	1    6150 1650
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J152
U 1 1 5C9225CD
P 6600 1650
F 0 "J152" H 6628 1580 50  0000 L CNN
F 1 "Conn_01x02_Female" H 6627 1535 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 6600 1650 50  0001 C CNN
F 3 "~" H 6600 1650 50  0001 C CNN
	1    6600 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J143
U 1 1 5C9225D3
P 5700 1750
F 0 "J143" H 5594 1517 50  0000 C CNN
F 1 "Conn_01x02_Female" H 5594 1516 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 5700 1750 50  0001 C CNN
F 3 "~" H 5700 1750 50  0001 C CNN
	1    5700 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 1750 5950 1750
Wire Wire Line
	5950 1750 5950 1650
Wire Wire Line
	5950 1650 6000 1650
Wire Wire Line
	5950 1650 5900 1650
Connection ~ 5950 1650
Wire Wire Line
	6300 1650 6350 1650
Wire Wire Line
	6400 1750 6350 1750
Wire Wire Line
	6350 1750 6350 1650
Connection ~ 6350 1650
Wire Wire Line
	6350 1650 6400 1650
$Comp
L Device:R R15
U 1 1 5C9225E3
P 6150 2050
F 0 "R15" V 5943 2050 50  0000 C CNN
F 1 "0603-1206" V 6034 2050 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 6080 2050 50  0001 C CNN
F 3 "~" H 6150 2050 50  0001 C CNN
	1    6150 2050
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J153
U 1 1 5C9225E9
P 6600 2050
F 0 "J153" H 6628 1980 50  0000 L CNN
F 1 "Conn_01x02_Female" H 6627 1935 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 6600 2050 50  0001 C CNN
F 3 "~" H 6600 2050 50  0001 C CNN
	1    6600 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J145
U 1 1 5C9225EF
P 5700 2150
F 0 "J145" H 5594 1917 50  0000 C CNN
F 1 "Conn_01x02_Female" H 5594 1916 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 5700 2150 50  0001 C CNN
F 3 "~" H 5700 2150 50  0001 C CNN
	1    5700 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 2150 5950 2150
Wire Wire Line
	5950 2150 5950 2050
Wire Wire Line
	5950 2050 6000 2050
Wire Wire Line
	5950 2050 5900 2050
Connection ~ 5950 2050
Wire Wire Line
	6300 2050 6350 2050
Wire Wire Line
	6400 2150 6350 2150
Wire Wire Line
	6350 2150 6350 2050
Connection ~ 6350 2050
Wire Wire Line
	6350 2050 6400 2050
$Comp
L Device:R R16
U 1 1 5C9225FF
P 6150 2400
F 0 "R16" V 5943 2400 50  0000 C CNN
F 1 "0603-1206" V 6034 2400 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 6080 2400 50  0001 C CNN
F 3 "~" H 6150 2400 50  0001 C CNN
	1    6150 2400
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J154
U 1 1 5C922605
P 6600 2400
F 0 "J154" H 6628 2330 50  0000 L CNN
F 1 "Conn_01x02_Female" H 6627 2285 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 6600 2400 50  0001 C CNN
F 3 "~" H 6600 2400 50  0001 C CNN
	1    6600 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J147
U 1 1 5C92260B
P 5700 2500
F 0 "J147" H 5594 2267 50  0000 C CNN
F 1 "Conn_01x02_Female" H 5594 2266 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 5700 2500 50  0001 C CNN
F 3 "~" H 5700 2500 50  0001 C CNN
	1    5700 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 2500 5950 2500
Wire Wire Line
	5950 2500 5950 2400
Wire Wire Line
	5950 2400 6000 2400
Wire Wire Line
	5950 2400 5900 2400
Connection ~ 5950 2400
Wire Wire Line
	6300 2400 6350 2400
Wire Wire Line
	6400 2500 6350 2500
Wire Wire Line
	6350 2500 6350 2400
Connection ~ 6350 2400
Wire Wire Line
	6350 2400 6400 2400
$Comp
L Device:R R17
U 1 1 5C92261B
P 6150 2750
F 0 "R17" V 5943 2750 50  0000 C CNN
F 1 "0603-1206" V 6034 2750 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 6080 2750 50  0001 C CNN
F 3 "~" H 6150 2750 50  0001 C CNN
	1    6150 2750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J155
U 1 1 5C922621
P 6600 2750
F 0 "J155" H 6628 2680 50  0000 L CNN
F 1 "Conn_01x02_Female" H 6627 2635 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 6600 2750 50  0001 C CNN
F 3 "~" H 6600 2750 50  0001 C CNN
	1    6600 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J148
U 1 1 5C922627
P 5700 2850
F 0 "J148" H 5594 2617 50  0000 C CNN
F 1 "Conn_01x02_Female" H 5594 2616 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 5700 2850 50  0001 C CNN
F 3 "~" H 5700 2850 50  0001 C CNN
	1    5700 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 2850 5950 2850
Wire Wire Line
	5950 2850 5950 2750
Wire Wire Line
	5950 2750 6000 2750
Wire Wire Line
	5950 2750 5900 2750
Connection ~ 5950 2750
Wire Wire Line
	6300 2750 6350 2750
Wire Wire Line
	6400 2850 6350 2850
Wire Wire Line
	6350 2850 6350 2750
Connection ~ 6350 2750
Wire Wire Line
	6350 2750 6400 2750
$Comp
L Device:R R18
U 1 1 5C922637
P 6150 3150
F 0 "R18" V 5943 3150 50  0000 C CNN
F 1 "0603-1206" V 6034 3150 50  0000 C CNN
F 2 "custom-footprints:SMT_0603-1206" V 6080 3150 50  0001 C CNN
F 3 "~" H 6150 3150 50  0001 C CNN
	1    6150 3150
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J156
U 1 1 5C92263D
P 6600 3150
F 0 "J156" H 6628 3080 50  0000 L CNN
F 1 "Conn_01x02_Female" H 6627 3035 50  0001 L CNN
F 2 "custom-footprints:2-pin" H 6600 3150 50  0001 C CNN
F 3 "~" H 6600 3150 50  0001 C CNN
	1    6600 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J149
U 1 1 5C922643
P 5700 3250
F 0 "J149" H 5594 3017 50  0000 C CNN
F 1 "Conn_01x02_Female" H 5594 3016 50  0001 C CNN
F 2 "custom-footprints:2-pin" H 5700 3250 50  0001 C CNN
F 3 "~" H 5700 3250 50  0001 C CNN
	1    5700 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 3250 5950 3250
Wire Wire Line
	5950 3250 5950 3150
Wire Wire Line
	5950 3150 6000 3150
Wire Wire Line
	5950 3150 5900 3150
Connection ~ 5950 3150
Wire Wire Line
	6300 3150 6350 3150
Wire Wire Line
	6400 3250 6350 3250
Wire Wire Line
	6350 3250 6350 3150
Connection ~ 6350 3150
Wire Wire Line
	6350 3150 6400 3150
$Comp
L Connector:Conn_01x04_Female J157
U 1 1 5C9E830B
P 2200 6300
F 0 "J157" V 2300 6350 50  0000 L CNN
F 1 "Conn_01x04_Female" V 2138 6448 50  0001 L CNN
F 2 "custom-footprints:4-pin" H 2200 6300 50  0001 C CNN
F 3 "~" H 2200 6300 50  0001 C CNN
	1    2200 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 6100 2000 6050
Wire Wire Line
	2000 6050 2100 6050
Wire Wire Line
	2100 6100 2100 6050
Connection ~ 2100 6050
Wire Wire Line
	2100 6050 2200 6050
Wire Wire Line
	2200 6100 2200 6050
Connection ~ 2200 6050
Wire Wire Line
	2200 6050 2300 6050
Wire Wire Line
	2300 6100 2300 6050
$Comp
L Connector:Conn_01x04_Female J158
U 1 1 5C9E831A
P 2200 6750
F 0 "J158" V 2300 6800 50  0000 L CNN
F 1 "Conn_01x04_Female" V 2138 6898 50  0001 L CNN
F 2 "custom-footprints:4-pin" H 2200 6750 50  0001 C CNN
F 3 "~" H 2200 6750 50  0001 C CNN
	1    2200 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 6550 2000 6500
Wire Wire Line
	2000 6500 2100 6500
Wire Wire Line
	2100 6550 2100 6500
Connection ~ 2100 6500
Wire Wire Line
	2100 6500 2200 6500
Wire Wire Line
	2200 6550 2200 6500
Connection ~ 2200 6500
Wire Wire Line
	2200 6500 2300 6500
Wire Wire Line
	2300 6550 2300 6500
$Comp
L Connector:Conn_01x04_Female J159
U 1 1 5C9E8329
P 2200 7200
F 0 "J159" V 2300 7250 50  0000 L CNN
F 1 "Conn_01x04_Female" V 2138 7348 50  0001 L CNN
F 2 "custom-footprints:4-pin" H 2200 7200 50  0001 C CNN
F 3 "~" H 2200 7200 50  0001 C CNN
	1    2200 7200
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 7000 2000 6950
Wire Wire Line
	2000 6950 2100 6950
Wire Wire Line
	2100 7000 2100 6950
Connection ~ 2100 6950
Wire Wire Line
	2100 6950 2200 6950
Wire Wire Line
	2200 7000 2200 6950
Connection ~ 2200 6950
Wire Wire Line
	2200 6950 2300 6950
Wire Wire Line
	2300 7000 2300 6950
$Comp
L Connector:Conn_01x04_Female J160
U 1 1 5C9E8338
P 2200 7650
F 0 "J160" V 2300 7700 50  0000 L CNN
F 1 "Conn_01x04_Female" V 2138 7798 50  0001 L CNN
F 2 "custom-footprints:4-pin" H 2200 7650 50  0001 C CNN
F 3 "~" H 2200 7650 50  0001 C CNN
	1    2200 7650
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 7450 2000 7400
Wire Wire Line
	2000 7400 2100 7400
Wire Wire Line
	2100 7450 2100 7400
Connection ~ 2100 7400
Wire Wire Line
	2100 7400 2200 7400
Wire Wire Line
	2200 7450 2200 7400
Connection ~ 2200 7400
Wire Wire Line
	2200 7400 2300 7400
Wire Wire Line
	2300 7450 2300 7400
Wire Wire Line
	1650 5400 1650 5700
$EndSCHEMATC
