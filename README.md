# Nifty PCB: SMT Breakout Breadboard


This Nifty PCB has space for three SOT-23-6 parts, and two SOIC-14 parts -- conveniently broken out to an array of through-holes, joined in rows like a normal breadboard. Also has several spaces for standard SMT parts ranging in size (imperial) from 0603 to 1206. Has two banana plug compatible mounting holes, for supplying power to the rails. 


![Schematic](./schematic.svg)

_**NOTE:** The schematic above uses a 74HC00 part for the SOIC-14 footprint - this is just to get the right footprint for the PCB, of course any part can be used!_

![Front of PCB](./front.png)

![Back of PCB](./back.png)

# Licence

All hardware schematics and design files are released under the CERN Open Hardware Licence v1.2. Please see [LICENCE](LICENCE) for details.
